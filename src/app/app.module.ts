import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { HelperService } from 'src/app/services/helper.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { NavComponent } from './layout/nav/nav.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { MlpComponent } from './pages/mlp/mlp.component';
import { AlwaysComponent } from './pages/always/always.component';
import { DiveComponent } from './pages/dive/dive.component';
import { TrabalhosComponent } from './pages/trabalhos/trabalhos.component';
import { ContatoComponent } from './pages/contato/contato.component';
import { TrabalheconoscoComponent } from './pages/trabalheconosco/trabalheconosco.component';
import { NosComponent } from './pages/nos/nos.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AssistirComponent } from './pages/home/assistir/assistir.component';
import { OuvirComponent } from './pages/home/ouvir/ouvir.component';
import { LerComponent } from './pages/home/ler/ler.component';
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    HomeComponent,
    MlpComponent,
    AlwaysComponent,
    DiveComponent,
    TrabalhosComponent,
    ContatoComponent,
    TrabalheconoscoComponent,
    NosComponent,
    AssistirComponent,
    OuvirComponent,
    LerComponent
  ],
  imports: [
    BrowserModule,
    SlickCarouselModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
  ],
  providers: [HelperService],
  bootstrap: [AppComponent]
})
export class AppModule { }
