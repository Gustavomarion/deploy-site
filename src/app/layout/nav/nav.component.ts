import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  public solucoes = false;
  public produtos = false;
  public conteudos = false;
  public menu = false;
  public solu = false;
  public cont = false;

  constructor(public helper: HelperService) { }

  ngOnInit() {
  }
  dropSolucoes() {
    const s = document.getElementById('solucoes') as HTMLBodyElement;
    const img = document.getElementById('solucoes-img') as HTMLImageElement;
    this.solucoes = !this.solucoes;
    if (this.solucoes === true) {
      img.src = `./assets/icons/purple_dropdown.svg`;
      s.style.color = '#8D4BBA';
      this.produtos = false;
      this.conteudos = false;
    } else {
      img.src = `./assets/icons/dropdown.svg`;
      s.style.color = '#000000';
    }
  }

  dropProdutos() {
    const p = document.getElementById('produtos') as HTMLBodyElement;
    const img = document.getElementById('produtos-img') as HTMLImageElement;

    this.produtos = !this.produtos;
    if (this.produtos === true) {
      img.src = `./assets/icons/purple_dropdown.svg`;
      p.style.color = '#8D4BBA';
      this.solucoes = false;
      this.conteudos = false;
    } else {
      img.src = `./assets/icons/dropdown.svg`;
      p.style.color = '#000000';
    }
  }

  dropConteudos() {
    const c = document.getElementById('conteudos') as HTMLBodyElement;
    const img = document.getElementById('conteudos-img') as HTMLImageElement;

    this.conteudos = !this.conteudos;
    if (this.conteudos === true) {
      img.src = `./assets/icons/purple_dropdown.svg`;
      c.style.color = '#8D4BBA';
      this.solucoes = false;
      this.produtos = false;
    } else {
      img.src = `./assets/icons/dropdown.svg`;
      c.style.color = '#000000';
    }
  }

  abrirMenu() {
    this.menu = !this.menu;
  }

  dropSolu() {
    const soluc = document.getElementById('icon-solu') as HTMLBodyElement;
    const icon = document.getElementById('iconS') as HTMLImageElement;
    const bg = document.getElementById('solu') as HTMLBodyElement;
    this.solu = !this.solu;
    if (this.solu === true) {
      icon.innerHTML = `&and;`;
      icon.src = './assets/icons/white_dropdown_up.svg',
      soluc.style.color = '#ffffff';
      bg.style.backgroundColor = '#391D4D';
    } else {
      icon.src = './assets/icons/white_dropdown_down.svg',
      bg.style.backgroundColor = '#8D4BBA';

    }
  }

  dropCont() {
    const contt = document.getElementById('icon-cont') as HTMLBodyElement;
    const iconC = document.getElementById('iconC') as HTMLImageElement;
    const bg = document.getElementById('content') as HTMLBodyElement;
    this.cont = !this.cont;
    if (this.cont === true) {
      iconC.src = './assets/icons/white_dropdown_up.svg',
      contt.style.color = '#ffffff';
      bg.style.backgroundColor = '#391D4D';
    } else {
      iconC.src = './assets/icons/white_dropdown_down.svg',
      bg.style.backgroundColor = '#8D4BBA';

    }
  }
}
