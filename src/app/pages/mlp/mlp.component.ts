import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mlp',
  templateUrl: './mlp.component.html',
  styleUrls: ['./mlp.component.scss']
})
export class MlpComponent implements OnInit {

  public idxOn: any;

  public imersao = false;
  public pesquisa = false;
  public prototipos = false;
  public testes = false;
  public desenvolvimento = false;

  constructor() { }

  ngOnInit() {
  }

  public Descoberta() {
    this.idxOn = 1;
    const img = document.getElementsByClassName('numero')[0] as HTMLImageElement;
    img.src = './assets/icons/search_icon.svg';

    const b = document.getElementsByClassName('numero')[1] as HTMLImageElement;
    b.src = './assets/photos/num02.png';
    const c = document.getElementsByClassName('numero')[2] as HTMLImageElement;
    c.src = './assets/photos/num03.png';
    const d = document.getElementsByClassName('numero')[3] as HTMLImageElement;
    d.src = './assets/photos/num04.png';
    const e = document.getElementsByClassName('numero')[4] as HTMLImageElement;
    e.src = './assets/photos/num05.png';
  }

  public Laboratorio() {
    this.idxOn = 2;
    const img = document.getElementsByClassName('numero')[1] as HTMLImageElement;
    img.src = './assets/icons/lab_icon.svg';

    const a= document.getElementsByClassName('numero')[0] as HTMLImageElement;
    a.src = './assets/photos/num01.png';
    const c = document.getElementsByClassName('numero')[2] as HTMLImageElement;
    c.src = './assets/photos/num03.png';
    const d = document.getElementsByClassName('numero')[3] as HTMLImageElement;
    d.src = './assets/photos/num04.png';
    const e = document.getElementsByClassName('numero')[4] as HTMLImageElement;
    e.src = './assets/photos/num05.png';
  }

  public Interface() {
    this.idxOn = 3;    /* Trocar para imagem do icone */
    const img = document.getElementsByClassName('numero')[2] as HTMLImageElement;
    img.src = './assets/icons/interface_icon.svg';

    const a= document.getElementsByClassName('numero')[0] as HTMLImageElement;
    a.src = './assets/photos/num01.png';
    const b = document.getElementsByClassName('numero')[1] as HTMLImageElement;
    b.src = './assets/photos/num02.png';
    const d = document.getElementsByClassName('numero')[3] as HTMLImageElement;
    d.src = './assets/photos/num04.png';
    const e = document.getElementsByClassName('numero')[4] as HTMLImageElement;
    e.src = './assets/photos/num05.png';
  }

  public Teste() {
    this.idxOn = 4;
    const img = document.getElementsByClassName('numero')[3] as HTMLImageElement;
    img.src = './assets/icons/user_check_icon.svg';

    const a= document.getElementsByClassName('numero')[0] as HTMLImageElement;
    a.src = './assets/photos/num01.png';
    const b = document.getElementsByClassName('numero')[1] as HTMLImageElement;
    b.src = './assets/photos/num02.png';
    const c = document.getElementsByClassName('numero')[2] as HTMLImageElement;
    c.src = './assets/photos/num03.png';
    const e = document.getElementsByClassName('numero')[4] as HTMLImageElement;
    e.src = './assets/photos/num05.png';
  }

  public Desenvolvimento() {
    this.idxOn = 5;
    const img = document.getElementsByClassName('numero')[4] as HTMLImageElement;
    img.src = './assets/icons/web-design_icon.svg';

    const a= document.getElementsByClassName('numero')[0] as HTMLImageElement;
    a.src = './assets/photos/num01.png';
    const b = document.getElementsByClassName('numero')[1] as HTMLImageElement;
    b.src = './assets/photos/num02.png';
    const c = document.getElementsByClassName('numero')[2] as HTMLImageElement;
    c.src = './assets/photos/num03.png';
    const d = document.getElementsByClassName('numero')[3] as HTMLImageElement;
    d.src = './assets/photos/num04.png';
  }

  public OutDescoberta() {
    this.idxOn = 0;
    const img = document.getElementsByClassName('numero')[0] as HTMLImageElement;
    img.src = './assets/photos/num01.png';
  }
  public OutLaboratorio() {
    this.idxOn = 0;
    const img = document.getElementsByClassName('numero')[1] as HTMLImageElement;
    img.src = './assets/photos/num02.png';
  }
  public OutInterface() {
    this.idxOn = 0;
    const img = document.getElementsByClassName('numero')[2] as HTMLImageElement;
    img.src = './assets/photos/num03.png';
  }
  public OutTeste() {
    this.idxOn = 0;
    const img = document.getElementsByClassName('numero')[3] as HTMLImageElement;
    img.src = './assets/photos/num04.png';
  }
  public OutDesenvolvimento() {
    this.idxOn = 0;
    const img = document.getElementsByClassName('numero')[4] as HTMLImageElement;
    img.src = './assets/photos/num05.png';
  }

  dropImersao() {
    const bg = document.getElementById('imersao') as HTMLBodyElement;
    const img = document.getElementById('img-imersao') as HTMLImageElement;
    const plus = document.getElementsByClassName('plus')[0] as HTMLImageElement;
    this.imersao = !this.imersao;
    if (this.imersao === true) {
      img.src = './assets/icons/meeting_icon.svg';
      plus.src = './assets/icons/less_icon.svg';
      bg.style.backgroundColor = '#ffffff';
    } else {
      img.src = './assets/photos/num01.png';
      plus.src = './assets/icons/plus_icon.svg';
      bg.style.backgroundColor = '#F9F4FD';

    }
  }

  dropPesquisa() {
    const bg = document.getElementById('pesquisa') as HTMLBodyElement;
    const img = document.getElementById('img-pesquisa') as HTMLImageElement;
    const plus = document.getElementsByClassName('plus')[1] as HTMLImageElement;
    this.pesquisa = !this.pesquisa;
    if (this.pesquisa === true) {
      img.src = './assets/icons/lab_icon.svg';
      plus.src = './assets/icons/less_icon.svg';
      bg.style.backgroundColor = '#ffffff';
    } else {
      img.src = './assets/photos/num02.png';
      plus.src = './assets/icons/plus_icon.svg';
      bg.style.backgroundColor = '#F9F4FD';

    }
  }

  dropPrototipos() {
    const bg = document.getElementById('prototipos') as HTMLBodyElement;
    const img = document.getElementById('img-prototipos') as HTMLImageElement;
    const plus = document.getElementsByClassName('plus')[2] as HTMLImageElement;
    this.prototipos = !this.prototipos;
    if (this.prototipos === true) {
      img.src = './assets/icons/interface_icon.svg';
      plus.src = './assets/icons/less_icon.svg';
      bg.style.backgroundColor = '#ffffff';
    } else {
      img.src = './assets/photos/num03.png';
      plus.src = './assets/icons/plus_icon.svg';
      bg.style.backgroundColor = '#F9F4FD';

    }
  }

  dropTestes() {
    const bg = document.getElementById('testes') as HTMLBodyElement;
    const img = document.getElementById('img-testes') as HTMLImageElement;
    const plus = document.getElementsByClassName('plus')[3] as HTMLImageElement;
    this.testes = !this.testes;
    if (this.testes === true) {
      img.src = './assets/icons/user_check_icon.svg';
      plus.src = './assets/icons/less_icon.svg';
      bg.style.backgroundColor = '#ffffff';
    } else {
      img.src = './assets/photos/num04.png';
      plus.src = './assets/icons/plus_icon.svg';
      bg.style.backgroundColor = '#F9F4FD';

    }
  }

  dropDesenvolvimento() {
    const bg = document.getElementById('desenvolvimento') as HTMLBodyElement;
    const img = document.getElementById('img-desenvolvimento') as HTMLImageElement;
    const plus = document.getElementsByClassName('plus')[4] as HTMLImageElement;
    this.desenvolvimento = !this.desenvolvimento;
    if (this.desenvolvimento === true) {
      img.src = './assets/icons/web-design_icon.svg';
      plus.src = './assets/icons/less_icon.svg';
      bg.style.backgroundColor = '#ffffff';
    } else {
      img.src = './assets/photos/num05.png';
      plus.src = './assets/icons/plus_icon.svg';
      bg.style.backgroundColor = '#F9F4FD';

    }
  }
}
