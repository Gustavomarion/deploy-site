import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contato',
  templateUrl: './contato.component.html',
  styleUrls: ['./contato.component.scss']
})
export class ContatoComponent implements OnInit {

  public form: FormGroup = this.fb.group({
    subject: ['', Validators.required],
    name: ['', Validators.required],
    company: ['', Validators.required],
    email: ['', Validators.required],
    tel: ['', Validators.required],
    msg: ['', Validators.required],
    to_email: ['talk@deploy.com'],
    to_name: ['Deploy - Novo contato site'],
    cc_emails: [['msouza@marketingmanager.com.br', 'talk@deploy.com']],
  });

  constructor(
    private fb: FormBuilder,
    // private Cms: CmsService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  send() {
  //   this.cmsService.send(this.form.value).subscribe(() => {
  //   });
  }

}
