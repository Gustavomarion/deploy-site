import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OuvirComponent } from './ouvir.component';

describe('OuvirComponent', () => {
  let component: OuvirComponent;
  let fixture: ComponentFixture<OuvirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OuvirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OuvirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
