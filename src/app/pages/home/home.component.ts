import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HelperService } from 'src/app/services/helper.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    @ViewChild('video', { static: false }) public video: ElementRef;
    public form: FormGroup = this.fb.group({
        email: ['', [Validators.required, Validators.email]],
        subject: ['Cadastro Workshop', Validators.required]
    });

    public jobs: any[] = [
        {
            id: 1,
            logo: './assets/logos/corona_logo.svg',
            title: 'Landing Page',
            video: 'http://marketingmanager.com.br/videos-deploy/corona.mp4',
            mobile: false,
            categories: [
                {
                    color: 'primary',
                    name: 'Categoria 1'
                },
                {
                    color: 'secondary',
                    name: 'Categoria 2'
                },
                {
                    color: 'tertiary',
                    name: 'Categoria 3'
                },
            ]
            // img: './assets/cases/Corona/Deploy-Case-Corona-Desktop.png'
        },
        {
            id: 2,
            logo: './assets/logos/red-nose_logo.svg',
            title: 'E-Commerce',
            video: 'http://marketingmanager.com.br/videos-deploy/red.mp4',
            mobile: true,
            categories: [
                
                {
                    color: 'secondary',
                    name: 'Categoria 2'
                },
                {
                    color: 'primary',
                    name: 'Categoria 1'
                },
                {
                    color: 'tertiary',
                    name: 'Categoria 3'
                },
            ]
        },
        {
            id: 3,
            logo: './assets/logos/rossi_logo.svg',
            video: 'http://marketingmanager.com.br/videos-deploy/4student.mp4',
            title: 'Website',
            mobile: true,
            categories: [
                {
                    color: 'tertiary',
                    name: 'Categoria 3'
                },
                {
                    color: 'secondary',
                    name: 'Categoria 2'
                },
                {
                    color: 'primary',
                    name: 'Categoria 1'
                },
                
            ]
        },
        {
            id: 4,
            logo: './assets/logos/kipling_logo.svg',
            video: 'http://marketingmanager.com.br/videos-deploy/red.mp4',
            title: 'App',
            mobile: true,
            categories: [
                {
                    color: 'secondary',
                    name: 'Categoria 2'
                },
                {
                    color: 'tertiary',
                    name: 'Categoria 3'
                },
                
                {
                    color: 'primary',
                    name: 'Categoria 1'
                },
                
            ]
        },
        {
            id: 5,
            logo: './assets/logos/colorado_logo.svg',
            video: 'http://marketingmanager.com.br/videos-deploy/volt.mp4',
            title: 'Landing Page + Sistema',
            mobile: false,
            categories: [
                {
                    color: 'tertiary',
                    name: 'Categoria 3'
                },
                {
                    color: 'secondary',
                    name: 'Categoria 2'
                },
                
                {
                    color: 'primary',
                    name: 'Categoria 1'
                },
                
            ]
        },
        {
            id: 6,
            logo: './assets/logos/hoegaarden_logo.svg',
            video: 'http://marketingmanager.com.br/videos-deploy/corona.mp4',
            title: 'Landing Page + Sistema',
            mobile: false,
            categories: [
                {
                    color: 'tertiary',
                    name: 'Categoria 3'
                },
                {
                    color: 'secondary',
                    name: 'Categoria 2'
                },
                
                {
                    color: 'primary',
                    name: 'Categoria 1'
                },
                
            ]
        }
    ];

    public idxActive: number = 0;
    constructor(private fb: FormBuilder, private route: ActivatedRoute, public helper: HelperService) { }


    get vdPlayer() {
        return this.video? this.video.nativeElement as HTMLVideoElement: {play(){}} as HTMLVideoElement;
    }
    public scroll(id, effect = true): void {
        const el = document.getElementById(id);
        if (effect) {
            el.scrollIntoView({
                behavior: 'smooth'
            });
        } else {
            el.scrollIntoView();
        }
    }

    ngOnInit() {
        this.route.params.subscribe((params: any) => {
            if (params.section) {
                this.scroll(params.section);
            }
        });


        if(this.helper.isMobile) {
            this.idxActive = null;
        }
     
    }

    enviar() {
        /*this.cmsService.send(this.form.value).subscribe(() => {});*/
    }

    public play(idx) {
        this.idxActive = idx;
        this.vdPlayer.src = this.jobs[idx].video;
        this.vdPlayer.play()

        if(this.helper.isMobile) {
            const body = document.getElementsByTagName('body')[0];
            body.style.overflow = 'hidden';
        }
    }
    public dismiss(e) {
        let has = (e.target as HTMLElement).classList.contains('video-model');
        const body = document.getElementsByTagName('body')[0];
        if(has && this.helper.isMobile) {
            this.idxActive = null;
            body.style.overflow = 'auto';
        }
    }
    Assistir() {
        const assistir = document.getElementById('assistir') as HTMLImageElement;
        const ouvir = document.getElementById('ouvir') as HTMLImageElement;
        const ler = document.getElementById('ler') as HTMLImageElement;

        assistir.src = './assets/icons/purple_play_icon.svg';
        ouvir.src = './assets/icons/speaker_icon.svg';
        ler.src = './assets/icons/read_icon.svg';

    }
    Ouvir() {
        const ouvir = document.getElementById('ouvir') as HTMLImageElement;
        const assistir = document.getElementById('assistir') as HTMLImageElement;
        const ler = document.getElementById('ler') as HTMLImageElement;

        ouvir.src = './assets/icons/purple_speaker_icon.svg';
        assistir.src = './assets/icons/play_icon.svg';
        ler.src = './assets/icons/read_icon.svg';
    }
    Ler() {
        const ler = document.getElementById('ler') as HTMLImageElement;
        const assistir = document.getElementById('assistir') as HTMLImageElement;
        const ouvir = document.getElementById('ouvir') as HTMLImageElement;

        ler.src = './assets/icons/purple_read_icon.svg';
        assistir.src = './assets/icons/play_icon.svg';
        ouvir.src = './assets/icons/speaker_icon.svg';
    }
}
