import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-assistir',
    templateUrl: './assistir.component.html',
    styleUrls: ['./assistir.component.scss']
})
export class AssistirComponent implements OnInit {

    public assistir: any = [
        {
            id: 1,
            img: './assets/photos/img3.png',
            description: 'O Marcus é o rei. Vamos todos Saudar o Marcus. O Marcus é o rei. Vamos todos Saudar o  Marcus',
        },
        {
            id: 2,
            img: './assets/photos/img1.png',
            description: 'O Marcus é o rei. Vamos todos Saudar o Marcus. O Marcus é o rei. Vamos todos Saudar o  Marcus',
        },
        {
            id: 3,
            img: './assets/photos/img2.png',
            description: 'O Marcus é o rei. Vamos todos Saudar o Marcus. O Marcus é o rei. Vamos todos Saudar o  Marcus',
        }
    ];
    constructor() { }

    public activeassistir: any = {};

    assistirConfig = {
        slidesToShow: 1,
        slidesToScroll: 1,
        Arrow: true,
        autoplaySpeed: 4500,
        autoplay: true,
    };

    public Slide(slide: any) {
        console.log(slide);
        this.activeassistir = slide;
    }
    ngOnInit() {
        this.activeassistir = this.assistir[0];
    }

    teste() { }
    slickInit(e) { }
    breakpoint(e) { }
    afterChange(e) {
        this.activeassistir = this.assistir[e.currentSlide];
    }
    beforeChange(e) { }

}
