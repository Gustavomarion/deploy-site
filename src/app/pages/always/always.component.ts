import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';

@Component({
    selector: 'app-always',
    templateUrl: './always.component.html',
    styleUrls: ['./always.component.scss']
})
export class AlwaysComponent implements OnInit {
    public items: any[] = [
        {
            name: 'Immersion',
            description: `Trough research methods, we develop a deep understanding of your users and business goals, metrics and painpoints`,
            transform: 'translate(-4%, -37%) rotateZ(1deg)',
            icon: './assets/always_beta/png/immersion.png',
            adjust_transform: 'rotateZ(-1deg)',
            text_adjust: {
                transform: 'translate(27%, 20%)',
                width: '54%'
            }
        },
        {
            name: 'Analyse',
            description: `Report generation to understand the efficiency of implementation, and keep improving the KPI's`,
            transform: 'translate(-34%, -15%) rotateZ(288deg)',
            icon: './assets/always_beta/png/analyse.png',
            adjust_transform: ' rotateZ(73deg)',
            text_adjust: {
                transform: 'translate(0%, 0%)',
                width: '27%'
            }
        },
        {
            name: 'Test',
            description: `With prototypes in hands, we run tests to get feedback from users.`,
            transform: 'translate(-22%, 20%) rotateZ(216deg)',
            icon: './assets/always_beta/png/test.png',
            adjust_transform: ' rotateZ(144deg)',
            text_adjust: {
                transform: 'translate(-7%, -25%)',
                width: '37%'
            }
        },

        {
            name: 'Ideate',
            description: `It's time to come up with many ideas as possible, filter them and prototype the best ones`,
            transform: 'translate(14%, 19%) rotateZ(144deg)',
            icon: './assets/always_beta/png/ideate.png',
            adjust_transform: ' rotateZ(-144deg)',
            text_adjust: {
                transform: 'translate(-12%, 18%)',
                width: '37%'
            }
        },
        {
            name: 'Define',
            description: `Now we analyze the data collected, identifying problems and opportunities then draw a roadmap with priorities.`,
            transform: 'translate(25%, -15%) rotateZ(73deg)',
            icon: './assets/always_beta/png/define.png',
            adjust_transform: 'rotateZ(-73deg)',
            text_adjust: {
                transform: 'translate(18%, 0%)',
                width: ' 29%'
            }
        },

    ]

    public ready: boolean;
    constructor(
        public helper: HelperService
    ) { }

    ngOnInit() {

        setTimeout(() => {
            this.ready = true;
        }, 500);
    }



    public takePositions(): any[] {
        let positions: any[] = [];
        this.items.forEach((item, idx) => {
            let idxNext = idx + 1;
            let idxLast = this.items.length - 1;

            if (idx == idxLast) {
                idxNext = 0;
            }

            positions.push({
                current: {
                    idx: idx,
                    adjust: this.items[idx].adjust_transform,
                    text_adjust: this.items[idx].text_adjust,
                    transform: this.items[idx].transform,
                },
                next: {
                    idx: idxNext,
                    adjust: this.items[idxNext].adjust_transform,
                    text_adjust: this.items[idxNext].text_adjust,
                    transform: this.items[idxNext].transform,
                }
            });
        });
        return positions;
    }
    public rotateLeft(e) {
        this.takePositions().forEach((val) => {
            this.items[val.current.idx].adjust_transform = val.next.adjust;
            this.items[val.current.idx].text_adjust = val.next.text_adjust;
            this.items[val.current.idx].transform = val.next.transform;
        });
    }
    public rotateRight(e) {
        this.takePositions().forEach((val) => {
            this.items[val.next.idx].adjust_transform = val.current.adjust;
            this.items[val.next.idx].text_adjust = val.current.text_adjust;
            this.items[val.next.idx].transform = val.current.transform;
        });

    }
}
