import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nos',
  templateUrl: './nos.component.html',
  styleUrls: ['./nos.component.scss']
})
export class NosComponent implements OnInit {
 public diferenciais: any = [
    {
      id: '01',
      margin: '-5px',
      image: './assets/photos/pesquisas.png',
      alt: 'pesquisas',
      title: 'Pesquisas e mais pesquisas',
      // tslint:disable-next-line: max-line-length
      description: 'Todo o nosso trabalho é voltado para identificar a melhor experiência possível para o seu público através de dados, sem achismo…'
    },
    {
      id: '02',
      margin: '5px',
      image: './assets/photos/cultivar.png',
      alt: 'cultivar',
      title: 'Buscamos cultivar relacionamentos',
      // tslint:disable-next-line: max-line-length
      description: 'Nada realmente valioso nessa vida se constrói sozinho, não é uma relação cliente x agência e sim uma combinação que forma um só time.'
    },
    {
      id: '03',
      margin: '10px',
      image: './assets/photos/rocket.png',
      alt: 'decolar',
      title: 'Queremos ver seu negócio decolar',
      // tslint:disable-next-line: max-line-length
      description: 'Nosso objetivo não é só entregar e pronto. Nos preocupamos com os detalhes e vamos além para trazer soluções pensadas para a visão do seu negócio.'
    },
    {
      id: '04',
      margin: '5px',
      image: './assets/photos/transparencia.png',
      alt: 'transparência',
      title: 'Papo reto e transparência',
      // tslint:disable-next-line: max-line-length
      description: 'Nossa metodologia é pensada para ser simples e eficiente. Durante cada etapa do projeto você terá acesso aos documentos e procedimentos de maneira fácil e descomplicada'
    },
    {
      id: '05',
      margin: '-5px',
      color: '#FF87BE',
      image: './assets/photos/estetica.png',
      alt: 'estética',
      title: 'Não é só sobre estética',
      cont: ' (mas é também)',
      // tslint:disable-next-line: max-line-length
      description: 'Desenvolvemos interfaces com as principais tendências de design da atualidade, sempre mantendo a coerência com seu público'
    }
 ];
 public idxOn = 0;

  constructor() { }
  public activeDiferenciais: any  = {};

  diferenciaisConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: ``,
    prevArrow: ``,
    autoplay: true,
    autoplaySpeed: 6000,
  };

  public Slide(slide: any) {
    console.log(slide);
    this.activeDiferenciais = slide;
  }

  ngOnInit() {
      this.activeDiferenciais = this.diferenciais[0];
  }

  teste() {}

  slickInit(e) {}

  breakpoint(e) {}

  afterChange(e) {
      this.activeDiferenciais = this.diferenciais[e.currentSlide];
      this.idxOn = e.currentSlide;
  }

  beforeChange(e) { }

}
