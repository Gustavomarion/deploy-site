import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
  public navType = 'nav-trasparent';
  public hasFooter = true;
  public hasNav = true;
  public cadastro = true;
  public isMobile: boolean;
  public loading = new BehaviorSubject(false);
  public navOpen: boolean;
  public content = 'nav';

  public userToLogin: any;
  hasCadastro: boolean;

  constructor(
    private breakpointObserver: BreakpointObserver,

  ) {
    this.breakpointObserver.observe(['(min-width: 993px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.isMobile = false;
        } else {
          this.isMobile = true;
        }
      });
  }

  public scroll() {
    parent.scroll(0, 0);
  }
}
