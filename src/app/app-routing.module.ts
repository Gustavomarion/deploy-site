import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { NosComponent } from './pages/nos/nos.component';
import { MlpComponent } from './pages/mlp/mlp.component';
import { AlwaysComponent } from './pages/always/always.component';
import { DiveComponent } from './pages/dive/dive.component';
import { TrabalhosComponent } from './pages/trabalhos/trabalhos.component';
import { TrabalheconoscoComponent } from './pages/trabalheconosco/trabalheconosco.component';
import { ContatoComponent } from './pages/contato/contato.component';
import { AssistirComponent } from './pages/home/assistir/assistir.component';
import { OuvirComponent } from './pages/home/ouvir/ouvir.component';
import { LerComponent } from './pages/home/ler/ler.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  {path: 'home', component: HomeComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'inicio' },
      { path: 'inicio', component: AssistirComponent },
      { path: 'para-ouvir', component: OuvirComponent },
      { path: 'para-ler', component: LerComponent }
    ]
  },
  { path: 'nos', component: NosComponent },
  { path: 'mlp', component: MlpComponent },
  { path: 'always-on', component: AlwaysComponent },
  { path: 'dive', component: DiveComponent },
  { path: 'trabalhos', component: TrabalhosComponent },
  { path: 'trabalhe-conosco', component: TrabalheconoscoComponent },
  { path: 'contato', component: ContatoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
